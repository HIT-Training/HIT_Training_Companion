package net.franks_reich.hit_training_companion.service.protocol.messages

import com.beust.klaxon.Klaxon
import org.junit.Test

import org.junit.Assert.*

class GetWorkoutTemplateResponseTest {
    @Test
    fun generateMessageFromObject() {
        val workoutTemplates = arrayListOf(
                WorkoutTemplate("Test 1", 1),
                WorkoutTemplate("Test 2", 2),
                WorkoutTemplate("Test 3", 3)
        )

        val message = GetWorkoutTemplatesResponse(workoutTemplates)
        val jsonString = Klaxon().toJsonString(message)
        assertEquals(
                """{"operation" : "GetWorkoutTemplates", "workoutTemplates"""" +
                        """ : [{"id" : 1, "name" : "Test 1"}, {"id" : 2, "name" : "Test 2"}, {"id" : 3, "name" : "Test 3"}]}""",
                jsonString)
    }
}
