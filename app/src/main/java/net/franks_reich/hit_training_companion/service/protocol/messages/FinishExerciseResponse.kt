package net.franks_reich.hit_training_companion.service.protocol.messages

class FinishExerciseResponse(val hasNextExercise: Boolean) {
    val operation = "FinishExercise"
}
