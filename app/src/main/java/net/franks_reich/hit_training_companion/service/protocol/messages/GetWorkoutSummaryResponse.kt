package net.franks_reich.hit_training_companion.service.protocol.messages

class GetWorkoutSummaryResponse(val completedExercises: List<CompletedExercise>) {
    val operation = "GetWorkoutSummary"
}

class CompletedExercise(
        val name: String,
        val weight: String,
        val id: Int,
        val previousWeight: String,
        val previousRepetitions: Int,
        val weightUnit: String,
        val repetitions: Int)
