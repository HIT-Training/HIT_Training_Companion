package net.franks_reich.hit_training_companion.service.protocol

import android.util.Log
import com.beust.klaxon.Klaxon
import com.beust.klaxon.PathMatcher
import com.samsung.android.sdk.accessory.SASocket
import net.franks_reich.hit_training_companion.service.protocol.messages.*
import java.io.StringReader

object RequestHandler {
    fun process(message: String, socket: SASocket, channelId: Int) {
        Log.i(RequestHandler::class.qualifiedName, "Determining operation for message $message")

        val pathMatcher = object : PathMatcher {
            override fun onMatch(path: String, value: Any) {
                when (value as String) {
                    "GetWorkoutTemplates" ->
                        processGetWorkoutTemplatesOperation(socket, channelId)
                    "StartWorkout" ->
                        processStartWorkoutOperation(socket, channelId, message)
                    "StartExercise" ->
                        processStartExerciseOperation(socket, channelId, message)
                    "FinishExercise" ->
                        processFinishExerciseOperation(socket, channelId, message)
                    "GetWorkoutSummary" ->
                        processGetWorkoutSummaryOperation(socket, channelId, message)
                    else -> Log.e(RequestHandler::class.qualifiedName, "Unknown operation")
                }
            }

            override fun pathMatches(path: String): Boolean = path == "$.operation"
        }

        Klaxon().pathMatcher(pathMatcher).parseJsonObject(StringReader(message))
    }

    private fun processGetWorkoutTemplatesOperation(socket: SASocket, channelId: Int) {
        Log.i(
                RequestHandler::class.qualifiedName,
                "Processing GetWorkoutTemplates operation")

        val workoutTemplates = arrayListOf(
                WorkoutTemplate("Test 1", 1),
                WorkoutTemplate("Test 2", 2),
                WorkoutTemplate("Test 3", 3)
        )
        val response = GetWorkoutTemplatesResponse(workoutTemplates)
        val jsonString = Klaxon().toJsonString(response)
        socket.send(channelId, jsonString.toByteArray())
    }

    private fun processStartWorkoutOperation(
            socket: SASocket, channelId: Int, request: String)
    {
        Log.i(
                RequestHandler::class.qualifiedName,
                "Processing StartWorkout operation")

        val response = StartWorkoutResponse(3)
        val jsonString = Klaxon().toJsonString(response)
        socket.send(channelId, jsonString.toByteArray())
    }

    private fun processStartExerciseOperation(
            socket: SASocket, channelId: Int, request: String)
    {
        Log.i(
                RequestHandler::class.qualifiedName,
                "Processing StartExercise operation")

        val exercise = Exercise("Bench Press", "200.5", 5, "200", 11, "Lbs")
        val response = StartExerciseResponse(exercise)
        val jsonString = Klaxon().toJsonString(response)
        socket.send(channelId, jsonString.toByteArray())
    }

    private fun processFinishExerciseOperation(
            socket: SASocket, channelId: Int, request: String)
    {
        Log.i(
                RequestHandler::class.qualifiedName,
                "Processing FinishExercise operation")

        val response = FinishExerciseResponse(false)
        val jsonString = Klaxon().toJsonString(response)
        socket.send(channelId, jsonString.toByteArray())
    }

    private fun processGetWorkoutSummaryOperation(
            socket: SASocket, channelId: Int, request: String)
    {
        Log.i(
                RequestHandler::class.qualifiedName,
                "Processing GetWorkoutSummary operation")

        val completedExercises = arrayListOf(
                CompletedExercise("Bench Press", "200.5", 5, "200", 11, "Lbs", 6),
                CompletedExercise("Pull Ups", "20", 3, "20", 4, "Lbs", 6),
                CompletedExercise("Bench Press", "200.5", 5, "200", 11, "Lbs", 6))

        val response = GetWorkoutSummaryResponse(completedExercises)
        val jsonString = Klaxon().toJsonString(response)
        socket.send(channelId, jsonString.toByteArray())
    }
}