package net.franks_reich.hit_training_companion.service.protocol.messages

class StartWorkoutResponse(val workoutId: Int) {
    val operation = "StartWorkout"
}
