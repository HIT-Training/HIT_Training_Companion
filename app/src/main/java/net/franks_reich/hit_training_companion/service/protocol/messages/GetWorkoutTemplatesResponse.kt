package net.franks_reich.hit_training_companion.service.protocol.messages

class GetWorkoutTemplatesResponse(val workoutTemplates: List<WorkoutTemplate>) {
    val operation = "GetWorkoutTemplates"
}

class WorkoutTemplate(val name: String, val id: Int)
