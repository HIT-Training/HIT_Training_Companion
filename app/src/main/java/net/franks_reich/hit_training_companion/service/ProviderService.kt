package net.franks_reich.hit_training_companion.service

import android.content.Context
import android.util.Log
import com.samsung.android.sdk.accessory.SA
import com.samsung.android.sdk.accessory.SAAgentV2
import com.samsung.android.sdk.accessory.SAPeerAgent
import com.samsung.android.sdk.accessory.SASocket
import net.franks_reich.hit_training_companion.service.protocol.RequestHandler
import kotlin.concurrent.thread

class ProviderService(private val context: Context?) :
        SAAgentV2(ProviderService::class.qualifiedName, context, ServiceConnection::class.java)
{
    private var socket: ServiceConnection? = null

    init {
        val accessory = SA()
        try {
            accessory.initialize(context)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    override fun onServiceConnectionRequested(peerAgent: SAPeerAgent?) {
        super.onServiceConnectionRequested(peerAgent)
        peerAgent?.let {
            Log.i(ProviderService::class.qualifiedName, "Connection accepted")
            acceptServiceConnectionRequest(peerAgent)
        }
    }

    override fun onServiceConnectionResponse(peerAgent: SAPeerAgent?, socket: SASocket?, result: Int) {
        super.onServiceConnectionResponse(peerAgent, socket, result)
        when (result) {
            SAAgentV2.CONNECTION_SUCCESS -> {
                Log.i(ProviderService::class.qualifiedName, "Connection created")
                socket?.let {
                    this.socket = socket as ServiceConnection
                }
            }
            else -> {
                Log.e(ProviderService::class.qualifiedName, "Unexpected connection response: $result")
            }
        }
    }

    inner class ServiceConnection : SASocket(ServiceConnection::class.qualifiedName) {
        override fun onServiceConnectionLost(reason: Int) {
            socket = null
        }

        override fun onReceive(channelId: Int, data: ByteArray?) {
            data?.let {
                val message = String(data)
                Log.i(ServiceConnection::class.qualifiedName, "Message received: $message")
                val workingSocket = socket
                thread(true) {
                    workingSocket?.let {
                        RequestHandler.process(
                                message, workingSocket, getServiceChannelId(0))
                    }
                }
            }
        }

        override fun onError(channelId: Int, errorMessage: String?, errorCode: Int) {
            Log.e(ServiceConnection::class.qualifiedName, errorMessage)
        }
    }
}