package net.franks_reich.hit_training_companion.service.protocol.messages

class StartExerciseResponse(val exercise: Exercise) {
    val operation = "StartExercise"
}

class Exercise(
        val name: String,
        val weight: String,
        val id: Int,
        val previousWeight: String,
        val previousRepetitions: Int,
        val weightUnit: String)
